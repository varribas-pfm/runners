#!/bin/bash
#
# This file is part of runners
# https://gitlab.com/varribas-pfm/runners
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
# Scope: Master Thesis in Computer Vision <https://gitlab.com/groups/varribas-pfm>

set -e
set -u


## Configuration
# Dataset
DATASET_DIR=/media/datasets/dataset-tum

DATASET_OUTPUT=$DATASET_DIR/results.d/orbslam2
mkdir -p $DATASET_OUTPUT

# Executable PATHS
ORB_SLAM2_HOME=${ORB_SLAM2_HOME-$HOME/orbslam_ws/ORB_SLAM2}
calibration_for(){
	mode=$(echo $1 | grep -o 'freiburg.' | tail -c 2)
	case $mode in
	1|2|3)
		echo ${ORB_SLAM2_HOME}/Examples/Monocular/TUM${mode}.yaml
	;;
	*)
		echo "warning: calibration_for $1 has no candidates" >&2
		return 1
	;;
	esac
}



find $DATASET_DIR -maxdepth 1 -mindepth 1 -type d \
| while read dataset
do
	echo "PROCESING $dataset"
	datadir=${dataset%.*}
	dataname=${datadir##*/}
	dataname=$(basename $dataset)

	calibration=$(calibration_for $dataname)
	output_dir=$DATASET_OUTPUT

	runner_orbslam2_imgs.sh $dataset $calibration $output_dir

	groundtruth=$datadir/groundtruth.txt
	if [ -e $groundtruth ]
	then
		cp $groundtruth $DATASET_OUTPUT/${dataname}-gt.txt
	fi
done
