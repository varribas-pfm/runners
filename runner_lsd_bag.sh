#!/bin/sh
#
# This file is part of runners
# https://gitlab.com/varribas-pfm/runners
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
# Scope: Master Thesis in Computer Vision <https://gitlab.com/groups/varribas-pfm>
#
# Usage:
# runner_lsd_bag <bag file> [output pose file]

set -e
set -u

dataset=${1} #-/media/datasets/lsd/LSD_room.bag}

dataname=$(basename $dataset)
dataname=${dataname%.*}
ofile=${2-${dataname}-poses.txt}
ofps=$(dirname $ofile)/${dataname}-fps.txt

logfile=${3-${dataname}.log}


set +e
echo "Power up nodes"
rosrun pose2file pose4tum.py _file:=$ofile &
pid_pose2file=$!

rosrun pose2file fps_listener.py _file:=$ofps &
pid_fps=$!

# for demo datasets (ex: LSD_room)
#rosrun lsd_slam_core live_slam image:=/image_raw camera_info:camera_info &
# for TUM datasets
rosrun lsd_slam_core live_slam image:=/camera/rgb/image_color camera_info:=/camera/rgb/camera_info &
pid_lsd_slam=$!


killwait(){
	kill $1
	wait $1
}

killforce(){
	kill $1
	sleep 1

	if kill -0 $1 2>/dev/null
	then
		kill -9 $1
		sleep 3
	fi
}

echo "Execute dataset $dataname"
rosbag play --quiet --clock $dataset &
wait $!
sleep 3
killwait $pid_lsd_slam
killwait $pid_pose2file
killwait $pid_fps
echo "Dataset $dataname completed"

if ps -ef | grep -v grep | egrep -q 'pose2file|lsd_slam_core'
then
	echo "  warning: $dataname did not exit properly" >&2
	echo "  purging any process..." >&2
	ps -ef | grep -v grep | egrep 'pose2file|lsd_slam_core' | awk '{print $2}' | xargs kill -9
fi

