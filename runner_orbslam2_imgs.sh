#!/bin/sh
#
# This file is part of runners
# https://gitlab.com/varribas-pfm/runners
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
# Scope: Master Thesis in Computer Vision <https://gitlab.com/groups/varribas-pfm>
#
# Usage:
# runner_orbslam2_imgs <tum dataset directory> <tum calibration file> [output directory]

set -e
set -u

# gobal config
ORB_SLAM2_HOME=${ORB_SLAM2_HOME-$HOME/orbslam_ws/ORB_SLAM2}
VOCABULARY=${VOCABULARY-$ORB_SLAM2_HOME/Vocabulary/ORBvoc.txt}

dataset=${1} #-/media/datasets/dataset-tum/rgbd_dataset_freiburg1_360}
calibration=${2} #-$ORB_SLAM2_HOME/Examples/Monocular/TUM1.yaml} [1,2,3]
output_directory=${3-$PWD}

dataname=$(basename $dataset)

poses=${output_directory}/${dataname}-poses.txt
poses_ba=${output_directory}/${dataname}-poses-ba.txt
poses_kf=${output_directory}/${dataname}-poses-keyframes.txt
ofps=${output_directory}/${dataname}-fps.txt
logfile=${output_directory}/${dataname}.log


set +e
echo "Going to temp dir"
mkdir -p /tmp/orbslam2.run
cd /tmp/orbslam2.run

echo "Run mono_tum"
$ORB_SLAM2_HOME/Examples/Monocular/mono_tum $VOCABULARY $calibration $dataset 2>&1 | tee $logfile &
pid_orbslam2=$(pgrep --parent $$ mono_tum)

# patch for hang at "System Reseting" but allow other fast exits
# wait 5 minutes in periods (10s --> 300s)
wait_by_steps(){
  for i in `seq 1 30`
  do
    sleep 10
    ps | grep -q "$pid_orbslam2" || return 0
  done
  return 1
}
if wait_by_steps
then
failed=0

cp UncorrectedTrajectory.txt $poses
cp Trajectory.txt $poses_ba
cp KeyFrameTrajectory.txt $poses_kf
cp fps.txt $ofps
else
failed=1

kill -15 $pid_orbslam2
sleep 1
echo "\nHang patch says: program killed after 5min of nothing" >> $logfile
fi

find /tmp/orbslam2.run -delete

if [ ${failed} -eq 0 ]; then
echo "Dataset $dataname completed"
else
echo "Dataset $dataname FAILED. See $logfile for more info"
fi

