#!/bin/sh
#
# This file is part of runners
# https://gitlab.com/varribas-pfm/runners
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
# Scope: Master Thesis in Computer Vision <https://gitlab.com/groups/varribas-pfm>
#
# Usage:
# runner_svo_bag <bag file> [output pose file]
# Example:
# ./runner_svo_bag.sh /media/datasets/dataset-tum/rgbd_dataset_freiburg1_xyz.bag "$(rospack find svo_ros)/param/camera_tum1.yaml" /tmp/svo
# ./runner_svo_bag.sh /media/datasets/svo/airground_rig_s3_2013-03-18_21-38-48.bag "$(rospack find svo_ros)/param/camera_pinhole.yaml" /tmp/svo

set -e
set -u

# global config
dataset=${1} #-/media/datasets/dataset-tum/rgbd_dataset_freiburg1_xyz.bag}
calibration=${2} #-camera_tum1.yaml} [1,2,3]
output_directory=${3-$PWD}

dataname=$(basename $dataset)
dataname=${dataname%.bag}

poses=${output_directory}/${dataname}-poses.txt
ofps=${output_directory}/${dataname}-fps.txt
logfile=${output_directory}/${dataname}.log


echo "Power up nodes"
rosrun pose2file pose4tum.py _topic:=/svo/pose4tum _file:=$poses &
pid_pose2file=$!
rosrun pose2file posecov2pose.py _in_topic:=/svo/pose _out_topic:=/svo/pose4tum &
pid_marker2pose=$!

rosrun pose2file svo_info2fps.py in_topic:=/svo/info out_topic:=/svo/fps &
pid_svofps=$!
rosrun pose2file fps_listener.py _topic:=/svo/fps _file:=$ofps &
pid_fps=$!

# Patch for TUM datasets
if echo "$dataname" | grep -q 'rgbd_dataset'; then
	TUM_PATCH=1
	CAM_TOPIC=/camera/rgb/image_mono
	BAG_ARGS=''
	BAG_POST_ARGS="/camera/rgb/image_color:=/camera/rgb/image_raw"
	ROS_NAMESPACE=camera/rgb rosrun image_proc image_proc &
	pid_image_proc=$!
else
	TUM_PATCH=0
	CAM_TOPIC=/camera/image_raw
	BAG_ARGS=''
	BAG_POST_ARGS=''
fi

# SVO
ROS_NAMESPACE=svo rosparam load $calibration
ROS_NAMESPACE=svo rosparam load  "$(rospack find svo_ros)/param/vo_accurate.yaml"
#ROS_NAMESPACE=svo rosparam set 'init_min_inliers' 30
rosrun svo_ros vo \
  _cam_topic:=$CAM_TOPIC \
  _accept_console_user_input:=false \
  </dev/null &
ret=$?
pid_svo_ros=$!
sleep 3
echo "SVO status:" $?

sigterm(){
        local signals='-2 -15 -9'
        for signal in $signals
        do
                if ps | grep -q $1; then
			kill $signal $1
			sleep 2
		else
			break
		fi
        done
}

echo "Execute dataset $dataname"
rosbag play --quiet --clock $BAG_ARGS $dataset $BAG_POST_ARGS &
wait $!
sleep 3
sigterm $pid_svo_ros
if [ $TUM_PATCH -eq 1 ]; then
	sigterm $pid_image_proc
fi
sigterm $pid_marker2pose
sigterm $pid_pose2file
sigterm $pid_svofps
sigterm $pid_fps
echo "Dataset $dataname completed"
ps
echo
echo
