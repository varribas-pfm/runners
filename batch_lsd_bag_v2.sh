#!/bin/bash
#
# This file is part of runners
# https://gitlab.com/varribas-pfm/runners
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
# Scope: Master Thesis in Computer Vision <https://gitlab.com/groups/varribas-pfm>

set -e
set -u
#set -x

## Configuration
# Dataset
DATASET_DIR=/media/datasets/dataset-tum

DATASET_OUTPUT=$DATASET_DIR/results.d/lsd_slam
mkdir -p $DATASET_OUTPUT

find $DATASET_DIR -name '*.bag' \
| grep -f $DATASET_DIR/whitelist.list \
| grep -v -f $DATASET_DIR/blacklist.list \
| while read dataset

do
	echo "PROCESING $dataset"
	datadir=${dataset%.*}
	dataname=${datadir##*/}

	bagfile=$dataset
	groundtruth=$datadir/groundtruth.txt
	ofile=$DATASET_OUTPUT/${dataname}-poses.txt

	runner_lsd_bag.sh $bagfile $ofile

	if [ -e $groundtruth ]
        then
                cp $groundtruth $DATASET_OUTPUT/${dataname}-gt.txt
        fi
done
