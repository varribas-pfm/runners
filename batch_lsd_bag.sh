#!/bin/bash
#
# This file is part of runners
# https://gitlab.com/varribas-pfm/runners
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
# Scope: Master Thesis in Computer Vision <https://gitlab.com/groups/varribas-pfm>

set -e
set -u
#set -x

## Configuration
# Dataset
DATASET_DIR=/media/datasets/dataset-tum
WHITELIST_GREP='1_360|1_floor|1_desk|1_desk2|1_room'
WHITELIST_GREP='bag'

DATASET_OUTPUT=$DATASET_DIR/results.d/lsd_slam
mkdir -p $DATASET_OUTPUT

# Dependencies
#CATKIN_WS=~/lsd_ws
#source $CATKIN_WS/devel/setup.bash

EVALUATION_TOOLS_PATH=/media/datasets/dataset-tum/eval_tools
PATH=$PATH:$EVALUATION_TOOLS_PATH

VARRIBAS_TOOLS_PATH="$HOME/runners:$HOME/pyPathVisualizer:$HOME/eval_timestamp:."
PATH=$PATH:$VARRIBAS_TOOLS_PATH


find $DATASET_DIR -name '*.bag' \
| egrep $WHITELIST_GREP \
| grep -v -f /media/datasets/dataset-tum/blacklisted.list \
| while read dataset
do
	echo "PROCESING $dataset"
	datadir=${dataset%.*}
	dataname=${datadir##*/}

	bagfile=$dataset
	groundtruth=$datadir/groundtruth.txt
	ofile=$DATASET_OUTPUT/${dataname}-poses.txt
	logfile=$DATASET_OUTPUT/${dataname}.log

	runner_lsd_bag.sh $bagfile $ofile $logfile

	if [ -e $groundtruth ]
	then
		cp $groundtruth $DATASET_OUTPUT/${dataname}-gt.txt
d(){
		# tum eval (track error)
		ate=$(evaluate_ate.py $groundtruth $ofile || true)
		rpe=$(evaluate_rpe.py $groundtruth $ofile || true)
		cat<<EOF | tee $DATASET_OUTPUT/${dataname}-tum-eval.txt
$ate #ate
$rpe #rpe
EOF
		# varribas eval
		mplvisualizer.py $DATASET_OUTPUT/${dataname}-poses.png $groundtruth $ofile

		export RESULT_DIR=$DATASET_OUTPUT
		export GT_DIR=$DATASET_DIR
		if [ -f "$GT_DIR/$dataname/rgb.txt" ]; then
			echo "Timestamp evaluation of '$dataname'..."
			timestamp_checker.sh ${dataname} > $DATASET_OUTPUT/${dataname}-timestamp-stats.txt
			timestamp_offset.sh  ${dataname} > $DATASET_OUTPUT/${dataname}-timestamp-offset.txt
		fi
}
	fi
done

# global evaluation
#fps_evaluation.py $DATASET_OUTPUT | tee $DATASET_OUTPUT/fps_study.txt

