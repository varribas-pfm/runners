#!/bin/bash
#
# This file is part of runners
# https://gitlab.com/varribas-pfm/runners
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
# Scope: Master Thesis in Computer Vision <https://gitlab.com/groups/varribas-pfm>

set -e
set -u


## Configuration
# Dataset
DATASET_DIR=/media/datasets/dataset-tum

DATASET_OUTPUT=$DATASET_DIR/results.d/svo
mkdir -p $DATASET_OUTPUT

# Camera calibration configs (embedded)
cat<<EOF>/tmp/svo_tum1.yaml
cam_model: Pinhole
cam_width: 640
cam_height: 480
cam_fx: 517.306408
cam_fy: 516.469215
cam_cx: 318.643040
cam_cy: 255.313989
cam_d0: 0.262383
cam_d1: -0.953104
cam_d2: 1.163314
cam_d3: 0
EOF

cat<<EOF>/tmp/svo_tum2.yaml
cam_model: Pinhole
cam_width: 640
cam_height: 480
cam_fx: 520.908620
cam_fy: 521.007327
cam_cx: 325.141442
cam_cy: 249.701764
cam_d0: 0.231222
cam_d1: -0.784899
cam_d2: 0.917205
cam_d3: 0
EOF

cat<<EOF>/tmp/svo_tum3.yaml
cam_model: Pinhole
cam_width: 640
cam_height: 480
cam_fx: 535.4
cam_fy: 539.2
cam_cx: 320.1
cam_cy: 247.6
cam_d0: 0.0
cam_d1: 0.0
cam_d2: 0.0
cam_d3: 0.0
EOF

calibration_for(){
	mode=$(echo $1 | grep -o 'freiburg.' | tail -c 2)
	case $mode in
	1|2|3)
		echo /tmp/svo_tum${mode}.yaml
	;;
	*)
		echo "warning: calibration_for $1 has no candidates" >&2
		return 1
	;;
	esac
}


[ -f $DATASET_DIR/whitelist.list ] || echo '.*' > $DATASET_DIR/whitelist.list 
touch $DATASET_DIR/blacklist.list

find $DATASET_DIR -name '*.bag' \
| grep -f $DATASET_DIR/whitelist.list \
| grep -v -f $DATASET_DIR/blacklist.list \
| while read dataset
do
	datadir=$(dirname $dataset)
	bagfile=$(basename $dataset)
	dataname=${bagfile%.bag}
	echo "PROCESING $dataname"

	calibration=$(calibration_for $dataname)
	output_dir=$DATASET_OUTPUT

	runner_svo_bag.sh $dataset $calibration $output_dir

	groundtruth=$datadir/$dataname/groundtruth.txt
	if [ -e $groundtruth ]
	then
		cp $groundtruth $DATASET_OUTPUT/${dataname}-gt.txt
	fi
done
