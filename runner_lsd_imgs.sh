#!/bin/sh
#
# This file is part of runners
# https://gitlab.com/varribas-pfm/runners
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
# Scope: Master Thesis in Computer Vision <https://gitlab.com/groups/varribas-pfm>
#
# Usage:
# runner_lsd_imgs <images path> <camera calibration file> <output pose file>

set -e
set -u

dataset=${1} #-/media/datasets/dataset-tum/rgbd_dataset_freiburg1_360/rgb}
calibration=${2} #-$dataset/../calibration.txt}

#dataname=$(basename $dataset)
ofile=${3} #-${dataname}-poses.txt}

set +e
echo "Power up nodes"
rosrun pose2file pose4tum.py _file:=$ofile &
pid_pose2file=$!

rosrun lsd_slam_core dataset _files:=$dataset _calib:=$calibration _hz:=0 &
pid_lsd_slam=$!


echo "Execute dataset $dataset"
rosbag play --quiet --clock $dataset
sleep 3
kill $pid_lsd_slam
kill $pid_pose2file
sleep 1
echo "Dataset $dataset completed"

ps -ef | grep -v grep | egrep -q 'pose2file|lsd_slam' \
&& echo "  warning: $dataet did not exit properly"

